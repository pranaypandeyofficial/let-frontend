import { useState, useEffect} from 'react';
import { FaBars } from 'react-icons/fa'; 
import './othernavbar.css';
import { Link } from 'react-router-dom';
import { getImages, useClickOutside } from '../utils';

const OtherNav = () => {
  const [navLogo, setNavLogo] = useState<string>('');
  const [isMobile, setIsMobile] = useState(window.innerWidth <= 1000);
  const [menuDropdown, setotherMenuDropdown] = useState(false);

  const handleResize = () => {
    setIsMobile(window.innerWidth <= 1000);
  };

  const handleMenuDropdown = () => {
    setotherMenuDropdown(!menuDropdown);
  }

  useEffect(() => {
    window.addEventListener('resize', handleResize);
    return () => {
      window.removeEventListener('resize', handleResize);
    };
  }, []);

  useEffect(() => {
    let isMounted = true;

    getImages().then((data) => {
      try{
        if (isMounted && data &&  data['logo']){
          setNavLogo(data['logo'] as string);
        }
      }
      catch(err){
        console.error("Error while fetching logo image", err);
      }

    }).catch((err) => {
      console.error("Error while fetching logo image", err);
    });
}, []);

  useClickOutside(() => setotherMenuDropdown(false), '.OthNavbar');


  return (
    <nav className='OthNavbar'>
      <div className='OthNavbar--left'>
        <img className='Othwebsite--logo' src={navLogo} alt='lettfaktura' height={'30px'} />
        
      </div>


      <div className='0thNavbar--right'>

      {isMobile && (
          <FaBars className='Othhamburger-icon' size={"25px"} onClick={handleMenuDropdown} />
        )}

        {isMobile && menuDropdown && (
          <div className="OthmenuDropdown">
            <ul className='OthNavdropdown--ul'>
              <li>Home</li>
              <li>Order</li>
              <li>Our Customers</li>
              <li>About Us</li>
              <li><Link to={"/contact_us"} className="link">Contact Us</Link></li>
              <li>Other Programs</li>
              <li>More</li>
            </ul>
          </div>
          
        )}
        </div>

        {/* Conditionally render the menu based on screen width */}
        {!isMobile &&
         (

      <div className='OthNavbar--right'>
          <ul className='OthNav--ul'>
            <li>Home</li>
            <li>Order</li>
            <li>Our Customers</li>
            <li>About Us</li>
            <li><Link to={"/contact_us"}>Contact Us</Link></li>
            <li>Other Programs</li>
            <li>More</li>
          </ul>
          </div>
        )}

      
    </nav>
  );
}

export default OtherNav;