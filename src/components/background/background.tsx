import React from "react";
import axios from "axios";

const Background = () => {
  const [background, setBackground] = React.useState("");

  React.useEffect(() => {
    axios
      .get(`${process.env.REACT_APP_URL_PREFIX}/images`)
      .then((response) => {
        try {
          const data = response.data;
          if (data) {
            console.log(data["background"]);
            setBackground(data["background"]);
          } else {
            console.error("Invalid data received from the server.");
          }
        } catch (err) {
            console.log(err);
        }
      })
      .catch((reason: any) => {
        console.log("Error in fetching data from server", reason);
        
      });
  }, []);
  return (
    <>
      <div
        style={{
          position: "fixed",
          width: "100%",
          height: "100vh",
          backgroundImage: `url(${background})`,
          backgroundRepeat: "no-repeat",
          backgroundPosition: "fixed",
          backgroundSize: "cover",
          backgroundAttachment: "fixed",
          zIndex: "-10",
          transform: "translateZ(0)",
          WebkitTransform: "translateZ(0)",
        }}
      ></div>
    </>
  );
};

export default Background;
