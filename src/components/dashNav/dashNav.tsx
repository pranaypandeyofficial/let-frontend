import React, {useState, useEffect} from "react";
import './dashNav.css';
import { FaBars } from "react-icons/fa";
import { fetchLanguageData, Language, useClickOutside } from '../utils';

const DashNav: React.FC = ()=>{


    const [languages, setLanguages] = useState<Language[]>([{name: '', flag: ''}]);
    const [currentLanguage, setCurrentLanguage] = useState(languages[0]);
  
    const [showDropdown, setShowDropdown] = useState(false);

    const handleLanguageChange = (selectedLanguage: { name: string; flag: string }) => {
        setCurrentLanguage(selectedLanguage);
        setShowDropdown(false);
      };
      useEffect(() => {

        let isMounted = true;
      fetchLanguageData().then((data) => {
        if (isMounted && data && data.length > 0){
          setLanguages(data as Language[]);
        }
      });
     
    }, []);
    
      useEffect(() => {
        setCurrentLanguage(languages[0]);
      }, [languages]);
      
    useClickOutside(()=>{
        setShowDropdown(false);
    }, ".langDropdown");

    return (
        <div className="dashNav">
            <div className="dashNav--left">
                <div className="dashnav--dpholder"></div>
                <div className="profileText">
                    <div className="profile-name">John Andre</div>
                    <div className="profile-loc">Stanford AS</div>
                </div>

                <FaBars className='dashNav-hamburger-icon' size={"25px"} onClick={()=>{}} />
                
            </div>
            <div className="dashNav--right">
            <div className="langDropdown">
                <div
                    className="langDropdown__selected"
                    onClick={() => setShowDropdown(!showDropdown)}
                >
                    <p>{currentLanguage.name}</p>
                    <img
                    src={currentLanguage.flag}
                    alt={currentLanguage.name}
                    height={'20px'}
                    />
                </div>

                {showDropdown && (
                    <div className="langDropdown__options">
                    {languages.map((language, index) => (
                        <div
                        key={index}
                        className="langDropdown__option"
                        onClick={() => handleLanguageChange(language)}
                        >
                        <p>{language.name}</p>
                        <img src={language.flag} alt="" height={'20px'} />
                        </div>
                    ))}
                    </div>
                )}
            </div>
            </div>
        </div>
    );
};

export default DashNav;